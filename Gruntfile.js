module.exports = function(grunt) {
    require('jit-grunt')(grunt);
    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        concat: {
            src: [

            ]
        },
        less: {
            development: {
                options: {
                    paths: ['styles/css']
                },
                files: {
                    // destination file and source file
                    'styles/css/layout.css': 'styles/less/helper.less'
                }
            }
        },
        concat_css: {
            options: {
                // Task-specific options go here.
            },
			files: {
				'styles/css/layout.css': ['styles/css/base.css', 'styles/css/fonts.css', 'styles/css/helper.css', 'styles/css/grids.css', 'styles/css/modal.css']
			}
        },
        watch: {
            styles: {
                files: ['**/*.less'],
                tasks: ['less'],
                options: {
                    nospawn: true
                }
            },
        },
        copy: {
            main: {
                files: [
                    // includes files within path
                    {expand: true, cwd: './vendor/wa-floatbox/css/', src: ['**'], dest: './styles/css/'},
                    {expand: true, cwd: './vendor/wa-floatbox/js/', src: ['**'], dest: './js/'}
                ]
            }
        }
    });
    
    grunt.loadNpmTasks('grunt-concat-css', 'grunt-contrib-copy');
    grunt.registerTask('default', ['less', 'concat_css', 'watch', 'copy']);
}