<?php
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

// If necessary, modify the path in the require statement below to refer to the
// location of your Composer autoload.php file.
require 'vendor/autoload.php';

// Nome
if (isset($_POST['nome'])) {
    
    $nome = filter_input(INPUT_POST, 'nome');
    //$nome = $_POST['nome'];
        
}

if (!$nome) {
    
    $retorno = [
        'success' => 0,
        'message' => 'Preencha o Nome.'
    ];
    
    echo json_encode($retorno);
    exit;
    
}

// Empresa
if (isset($_POST['empresa'])) {
    
    $empresa = filter_input(INPUT_POST, 'empresa');
    //$empresa = $_POST['empresa'];
        
}

if (!$empresa) {
    
    $retorno = [
        'success' => 0,
        'message' => 'Preencha a Empresa.'
    ];
    
    echo json_encode($retorno);
    exit;
    
}

// E-mail
if (isset($_POST['email'])) {
    
    $email = filter_input(INPUT_POST, 'email');
    //$email = $_POST['email'];
        
}


if (!$email) {
    
    $retorno = [
        'success' => 0,
        'message' => 'Preencha o E-mail.'
    ];
    
    echo json_encode($retorno);
    exit;
    
}

// Telefone
if (isset($_POST['telefone'])) {
    
    $telefone = filter_input(INPUT_POST, 'telefone');
    //$telefone = $_POST['telefone'];
        
}


if (!$telefone) {
    
    $retorno = [
        'success' => 0,
        'message' => 'Preencha o Telefone.'
    ];
    
    echo json_encode($retorno);
    exit;
    
}

// Captcha
if (isset($_POST['captcha'])) {
    
    $captcha = filter_input(INPUT_POST, 'captcha');
    //$captcha = $_POST['captcha'];
        
}

/*
if (!$captcha) {
    
    $retorno = [
        'success' => 0,
        'message' => 'Preencha o Captcha.'
    ];
    
    echo json_encode($retorno);
    exit;
    
}

$secretKey = "6LcIqLYqAAAAAEBA7UPJZ0eSFC6LYdM-JI0zZM6r";

$url = 'https://www.google.com/recaptcha/api/siteverify?secret=' .urlencode($secretKey) .  '&response=' . urlencode($captcha);

$response = file_get_contents($url);

$responseKeys = json_decode($response, true);

if (!$responseKeys['success']) {
    
    $retorno = [
        'success' => 0,
        'message' => 'Verifique o Captcha e tente novamente.'
    ];
    
    echo json_encode($retorno);
    exit;
    
}
*/

$sender = 'naoresponda@rubrum.com.br';
$senderName = 'Rubrum | Site';

$usernameSmtp = 'AKIAZIQPLYZ5O5EZ75WQ';

$passwordSmtp = 'BJne1PO8cSQvHntjcACAbmyzHQX9k8oB1zkJPimx6ij0';

$configurationSet = 'ConfigSet';

$host = 'email-smtp.sa-east-1.amazonaws.com';
$port = 587;

$subject = 'Contato via site de ' . utf8_decode($nome);

$bodyHtml = '<h1>Dados do contato:</h1>
    <p>Nome: '.$nome.'</p>
    <p>Empresa:  '.$empresa.'</p>
    <p>E-mail:  '.$email.'</p>
    <p>Telefone:  '.$telefone.'</p>
    ';

$mail = new PHPMailer(true);

try {
    // Specify the SMTP settings.
    $mail->isSMTP();
    $mail->setFrom($sender, $senderName);
    $mail->Username   = $usernameSmtp;
    $mail->Password   = $passwordSmtp;
    $mail->Host       = $host;
    $mail->Port       = $port;
    $mail->SMTPAuth   = true;
    $mail->SMTPSecure = 'tls';
    //$mail->addCustomHeader('X-SES-CONFIGURATION-SET', $configurationSet);

    // Specify the message recipients.
    //$mail->addAddress('bruno@paintpack.com.br');
    //$mail->addAddress('thiago@paintpack.com.br');
    $mail->addAddress('contato@rubrum.com.br');
    $mail->addAddress('cristina@paintpack.com.br');
    
    $mail->addReplyTo($email);
    
    // You can also add CC, BCC, and additional To recipients here.

    // Specify the content of the message.
    $mail->isHTML(true);
    $mail->Subject    = $subject;
    $mail->Body       = $bodyHtml;
    //$mail->AltBody    = $bodyText;
    $mail->Send();
    
    $retorno = [
        'success' => 1,
        'message' => 'Contato enviado com sucesso.'
    ];
    
    echo json_encode($retorno);
    
} catch (phpmailerException $e) {
    
    $retorno = [
        'success' => 0,
        'message' => 'Ocorreu um erro, tente novamente: ' . $e->errorMessage()
    ];
    
    echo json_encode($retorno);
    
    //echo "Ocorreu um erro. {$e->errorMessage()}", PHP_EOL; //Catch errors from PHPMailer.
    
} catch (Exception $e) {
    
    $retorno = [
        'success' => 0,
        'message' => 'E-mail não enviado, tente novamente: ' . $mail->ErrorInfo
    ];
    
    echo json_encode($retorno);
    
    //echo "E-mail não enviado. {$mail->ErrorInfo}", PHP_EOL; //Catch errors from Amazon SES.
    
}