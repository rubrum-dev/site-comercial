jconfirm.defaults = {
    title: '',
    titleClass: '',
    type: 'default',
    typeAnimated: true,
    draggable: false,
    dragWindowGap: 15,
    dragWindowBorder: false,
    animateFromElement: false,
    content: '',
    escapeKey: 'cancel',
    defaultButtons: {
        ok: {
            text: '<span><i class="icon to-right fa fa-thumbs-up"></i> Ok</span>',
            btnClass: 'btn-default',
            action: function () {
            }
        },
        cancel: {
            text: '<span><i class="icon to-right fa fa-ban"></i> Cancelar</span>',
            btnClass: 'btn-default',
            action: function () {
            }
        },
    },
    contentLoaded: function(data, status, xhr){
    },
    icon: '',
    lazyOpen: false,
    bgOpacity: null,
    theme: 'light',
    animation: 'scale',
    closeAnimation: 'scale',
    animationSpeed: 300,
    animationBounce: 1,
    rtl: false,
    container: 'body',
    containerFluid: false,
    backgroundDismiss: false,
    backgroundDismissAnimation: 'shake',
    autoClose: false,
    closeIcon: true,
    closeIconClass: 'jconfirm-btn-close fa fa-times',
    watchInterval: 100,
    columnClass: '',
    scrollToPreviousElement: false,
    scrollToPreviousElementAnimate: false,
    offsetTop: 40,
    offsetBottom: 40,
    onContentReady: function () {},
    onOpenBefore: function () {
        $('body').addClass('no-scroll');
        $('.wrap').addClass('blurred');
        $('.jconfirm').css('overflow', 'auto');
    },
    onOpen: function() {
        $('body').addClass('no-scroll');
        $('.wrap').addClass('blurred');
        $('.jconfirm').css('overflow', 'auto');
    },
    onClose: function () {
        $('body').removeClass('no-scroll');
        $('.wrap').removeClass('blurred');
        $('.jconfirm').css('overflow', 'hidden');
    },
    onDestroy: function () {
        $('body').removeClass('no-scroll');
        $('.wrap').removeClass('blurred');
        $('.jconfirm').css('overflow', 'hidden');
    },
    onAction: function () {}
};