"use strict";

$(function () {
    $('.watch').on('click', function () {
        $.dialog({
            title: '',
            content: '<video autoplay controls="">' +
                '<source src="./videos/Rubrum_Vídeo_Standard.mp4" type="video/mp4">' +
                '</video>',
            animateFromElement: false,
            animation: 'scale',
            backgroundDismiss: true,
            closeIconClass: 'jconfirm-btn-close fa fa-times',
            containerFluid: false,
            columnClass: '',
            onOpenBefore: function () {
                $('body').addClass('no-scroll');
                $('.wrap').addClass('blurred');
            },
            onClose: function () {
                $('body').removeClass('no-scroll');
                $('.wrap').removeClass('blurred');
            }
        });
    });

    $('.demo, .request-budget').on('click', function () {
        $.confirm({
            title: '',
            content: '' +
                '<form action="#" method="POST" autocomplete="off" name="formDemo" id="formDemo" class="form-demo form-container">' +
                '<h3 class="txt-size-lg txt-x-bold txt-center margin-bottom-20">Uma demonstração personalizada para você</h3>' +
                '<p class="txt-size-sm txt-regular txt-center margin-bottom-40">Entraremos em contato para agendar uma demonstração do Rubrum e te enviar um orçamento.</p>' +
                '<div class="flexbox-container flexbox-group">' +
                '<div class="demo-img-left flex flex-6-large">' +
                '<figure>' +
                '<img src="/images/layout/sistema-desktop-demo.png" class="img-fluid" />' +
                '</figure>' +
                '</div>' +
                '<div class="form-content flex flex-fluid">' +
                '<div class="form-group flex-12-large">' +
                '<input type="text" placeholder="Nome" name="nome" id="nome" class="input-control " />' +
                '</div>' +
                '<div class="form-group flex-12-large">' +
                '<input type="text" placeholder="Empresa" name="empresa" id="empresa" class="input-control" />' +
                '</div>' +
                '<div class="form-group flex-12-large">' +
                '<input type="email" placeholder="E-mail" name="email" id="email" class="input-control" />' +
                '</div>' +
                '<div class="form-group flex-12-large">' +
                '<input type="tel" placeholder="Telefone" name="telefone" id="telefone" class="input-control" />' +
                '</div>' +
                '<div class="form-group flex-12-large recaptcha-container">' +
                '<div id="recaptcha1" class="recaptcha"></div>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</form>',
            animateFromElement: false,
            animation: 'scale',
            backgroundDismiss: true,
            closeIcon: true,
            closeIconClass: 'jconfirm-btn-close fa fa-times',
            containerFluid: false,
            columnClass: '',
            buttons: {
                formSubmit: {
                    text: '<span><i class="icon to-right fa fa-paper-plane"></i> Enviar Solicitação</span>',
                    btnClass: 'btn-enviar btn btn-block btn-default',
                    keys: ['enter'],
                    action: function () {
                        var nome = $('#nome').val(),
                            empresa = $('#empresa').val(),
                            email = $('#email').val(),
                            telefone = $('#telefone').val(),
                            emailRegex = /^([a-z0-9_\.\-])+\@(([a-z0-9\-])+\.)+([a-z0-9]{2,4})+$/,
                            errorDOM = $('<label></label>');

                        $(errorDOM).addClass('error');

                        if (nome === '') {
                            $('label.error').remove();
                            $('#nome').addClass('error');
                            $('#nome').after(errorDOM);
                            $(errorDOM).text('Nos diga o seu nome.');

                            return false;
                        } else if (empresa === '') {
                            $('label.error').remove();
                            $('#empresa').addClass('error');
                            $('#empresa').after(errorDOM);
                            $(errorDOM).text('Nos diga a sua empresa.');

                            return false;
                        } else if (email === '' || !emailRegex.test($('#email').val())) {
                            $('label.error').remove();
                            $('#email').addClass('error');
                            $('#email').after(errorDOM);
                            $(errorDOM).text('Nos diga o seu e-mail.');

                            return false;
                        } else if (telefone === '') {
                            $('label.error').remove();
                            $('#telefone').addClass('error');
                            $('#telefone').after(errorDOM);
                            $(errorDOM).text('Nos diga o seu telefone.');

                            return false;
                        }

                        //var v = grecaptcha.getResponse();

                        //if (v.length == 0) {
                        //    $('label.error').remove();
                        //    $('#recaptcha1').after(errorDOM);
                        //    $(errorDOM).text("Preencha o Captcha.");

                        //    return false;
                        //}

                        $('.lds').show();

                        $.ajax({
                            type: 'POST',
                            url: 'sendmail.php',
                            data: {
                                nome: nome,
                                empresa: empresa,
                                email: email,
                                telefone: telefone
                                //captcha: v
                            }, // or JSON.stringify ({name: 'jonas'}),
                            success: function (data) {
                                if (data.success) {

                                    //$('#recaptcha1').empty();

                                    $('.lds').hide();

                                    $.alert({
                                        title: 'Solicitação enviada, obrigado!',
                                        content: '' +
                                            '<p class="txt-size-sm txt-center margin-bottom-40">Nos falaremos em breve.</p>' +
                                            '<div class="flexbox-container flexbox-group flex-justify-center">' +
                                            '<span class="jconfirm-success-sign fa fa-smile-o"></span>' +
                                            '</div>',
                                        containerFluid: false,
                                        columnClass: '',
                                        buttons: {
                                            entendi: {
                                                text: '<span><i class="icon to-right fa fa-thumbs-up"></i> Combinado</span>',
                                                keys: ['enter'],
                                                action: function () {}
                                            }
                                        },
                                        onOpenBefore: function () {
                                            $('body').addClass('no-scroll');
                                            $('.wrap').addClass('blurred');
                                            $('.jconfirm').css('overflow', 'auto');
                                            $('.jconfirm-buttons').addClass('no-float txt-center');
                                        },
                                        onOpen: function () {
                                            $('body').addClass('no-scroll');
                                            $('.wrap').addClass('blurred');
                                            $('.jconfirm').css('overflow', 'auto');
                                            $('.jconfirm-buttons').addClass('no-float txt-center');
                                        },
                                        onDestroy: function () {
                                            $('body').removeClass('no-scroll');
                                            $('.wrap').removeClass('blurred');
                                            $('.jconfirm').css('overflow', 'hidden');
                                            $('.jconfirm-buttons').removeClass('no-float txt-center');
                                        }
                                    });

                                } else {
                                    $('.lds').hide();
                                    //$('#recaptcha1').empty();

                                    $.alert({
                                        title: 'Erro',
                                        content: '' +
                                            '<p class="txt-size-sm txt-center margin-bottom-40">' + data.message + '</p>' +
                                            '<div class="flexbox-container flexbox-group flex-justify-center">' +
                                            '<span class="jconfirm-success-sign fa fa-frown-o"></span>' +
                                            '</div>',
                                        containerFluid: false,
                                        columnClass: '',
                                        buttons: {
                                            entendi: {
                                                text: '<span><i class="icon to-right fa fa-check"></i> Ok</span>',
                                                keys: ['enter'],
                                                action: function () {}
                                            }
                                        },
                                        onOpenBefore: function () {
                                            $('body').addClass('no-scroll');
                                            $('.wrap').addClass('blurred');
                                            $('.jconfirm').css('overflow', 'auto');
                                            $('.jconfirm-buttons').addClass('no-float txt-center');
                                        },
                                        onOpen: function () {
                                            $('body').addClass('no-scroll');
                                            $('.wrap').addClass('blurred');
                                            $('.jconfirm').css('overflow', 'auto');
                                            $('.jconfirm-buttons').addClass('no-float txt-center');
                                        },
                                        onDestroy: function () {
                                            $('body').removeClass('no-scroll');
                                            $('.wrap').removeClass('blurred');
                                            $('.jconfirm').css('overflow', 'hidden');
                                            $('.jconfirm-buttons').removeClass('no-float txt-center');
                                        }
                                    });

                                }

                            },
                            //contentType: "application/json",
                            dataType: 'json'
                        });

                    }
                }
            },
            onContentReady: function () {
                var jc = this;

                this.$content.find('form').on('submit', function (e) {
                    e.preventDefault();

                    jc.$$formSubmit.trigger('click');
                });

                $(document).on('focus', 'input.error', function () {
                    $('label.error').remove();
                });

                $(document).on('click', 'label.error', function () {
                    $(this).remove();
                    $('input.error').focus();
                });

                var SPMaskBehavior = function (val) {
                        return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
                    },
                    spOptions = {
                        onKeyPress: function (val, e, field, options) {
                            field.mask(SPMaskBehavior.apply({}, arguments), options);
                        }
                    };

                $('#telefone').mask(SPMaskBehavior, spOptions);

                //grecaptcha.render("recaptcha1", {
                //    sitekey: "6LcIqLYqAAAAAIqEC4EXVTzGW04A3O46wPygEFg1",
                //    theme: "light"
                //});
            },
            onOpenBefore: function () {
                $('body').addClass('no-scroll');
                $('.jconfirm-box').addClass('jc-demo-container');
                $('.wrap').addClass('blurred');
            },
            onDestroy: function () {
                $('body').removeClass('no-scroll');
                $('.jconfirm-box').removeClass('jc-demo-container');
                $('.wrap').removeClass('blurred');
                $('#recaptcha1').empty();
            }
        });
    });

    AOS.init({
        offset: 0,
        once: true
    });

    window.addEventListener('scroll', function () {
        if (this.pageYOffset <= 0) {
            $('.myk-btn').removeClass('active');
            $('.myk-panel').removeClass('myk-show');
        }
        
        AOS.refresh();
    });

    $('#carousel-workflow').lightSlider({
        loop: false,
        auto: false,
        controls: false,
        item: 4,
        slideMove: 1,
        slideMargin: 0,
        pause: 10000,
        pauseOnHover: true,
        pager: true,
        galleryMargin: 30,
        thumbMargin: 30,
        enableTouch: true,
        enableDrag: true,
        responsive: [{
                breakpoint: 1401,
                settings: {
                    item: 3,
                    slideMove: 1
                }
            },
            {
                breakpoint: 1201,
                settings: {
                    item: 3,
                    slideMove: 1
                }
            },
            {
                breakpoint: 1140,
                settings: {
                    item: 3,
                    slideMove: 1
                }
            },
            {
                breakpoint: 1024,
                settings: {
                    item: 2,
                    slideMove: 1
                }
            },
            {
                breakpoint: 767,
                settings: {
                    item: 1,
                    slideMove: 1
                }
            }
        ]
    });
    
    var sliderPlanos = $('#carousel-planos').lightSlider({
        loop: true,
        auto: false,
        controls: false,
        pager: true,
        item: 3,
        slideMove: 1,
        slideMargin: 0,
        galleryMargin: 30,
        thumbMargin: 30,
        pause: 10000,
        pauseOnHover: true,
        enableTouch: true,
        enableDrag: true,
        adaptiveHeight: true,
        responsive: [{
                breakpoint: 1401,
                settings: {
                    item: 3,
                    slideMove: 1
                }
            },
            {
                breakpoint: 1201,
                settings: {
                    item: 3,
                    slideMove: 1
                }
            },
            {
                breakpoint: 1140,
                settings: {
                    item: 3,
                    slideMove: 1
                }
            },
            {
                breakpoint: 1024,
                settings: {
                    loop: false,
                    item: 2,
                    slideMove: 1
                }
            },
            {
                breakpoint: 767,
                settings: {
                    loop: false,
                    item: 1,
                    slideMove: 1
                }
            },
            {
                breakpoint: 360,
                settings: {
                    loop: false,
                    item: 1,
                    slideMove: 1
                }
            },
            {
                breakpoint: 320,
                settings: {
                    loop: false,
                    item: 1,
                    slideMove: 1
                }
            }
        ],
        onSliderLoad: function (el) {}
    });


    $(window).on('load resize orientationchange', function() {
        if (window.innerWidth >= 768 && window.innerWidth <= 1024) {
            sliderPlanos.goToSlide(0);
        } else {
            sliderPlanos.goToSlide(1);
        }
                
        sliderPlanos.refresh();
    });
    
    var sliderDepoimentos = $('#carousel-depoimentos').lightSlider({
        loop: false,
        item: 1,
        slideMove: 1,
        slideMargin: 40,
        controls: true,
        pager: true,
        enableTouch: true,
        enableDrag: true,
        adaptiveHeight: true,
        galleryMargin: null,
        thumbMargin: 30,
        responsive: [{
                breakpoint: 1024,
                settings: {
                    item: 1,
                    slideMove: 2,
                    galleryMargin: null,
                    thumbMargin: 30
                }
            },
            {
                breakpoint: 1023,
                settings: {
                    item: 1,
                    slideMove: 1,
                    galleryMargin: null,
                    thumbMargin: 0
                }
            },
            {
                breakpoint: 768,
                settings: {
                    item: 1,
                    slideMove: 1,
                    galleryMargin: null,
                    thumbMargin: 0
                }
            },
            {
                breakpoint: 767,
                settings: {
                    item: 1,
                    slideMove: 1,
                    galleryMargin: null,
                    thumbMargin: 0
                }
            }
        ]
    });

    $('.header-in').visualNav({
        link: 'a.menu-link',
        useHash: false,
        bottomMargin: 100,
        offsetTop: 40,
        animationTime: 800,
        changed: function(api) {
            horizontalTimelineSlider();
            setStartTimelineTransitionFrom(INITIAL_DOT);
        }
    });
        
    $(".myk-wa").WAFloatBox();

    // click outside the button
    $(document).on('click', function(e) {
        if(!e.target.closest(".myk-panel") && $(".myk-btn").hasClass("active")) {
            $(".myk-panel").removeClass("myk-show");
            $(".myk-btn").removeClass("active");
        }
    });

    // Solution for collapsible menu on landscape screen mobile
    //$( window ).on('resize load', function() {
    //    if(screen.availHeight < screen.availWidth){
    //        $('.demo-btn').css('marginTop', '70px');
    //    } else {
    //        $('.demo-btn').css('marginTop', '0px');
    //   }
    //});

    setTimeout(function() {
        history.replaceState('', document.title, window.location.origin + window.location.pathname + window.location.search);
    }, 5);

    function stickyNavigation() {
        var offset = $('#header').offset(),
            navParent = $('#header'),
            nav = navParent.find('div');

        $(window).on('load scroll', function () {
            if (this.pageYOffset > 0 || window.pageYOffset > offset.top) {
                $(navParent).addClass('scrolled');
                $(nav).addClass('header-fixed');
            } else {
                $(navParent).removeClass('scrolled');
                $(nav).removeClass('header-fixed');
            }
        });
    }

    function scrollNavigation() {
        var scrollPos = 0;

        $(window).on('load scroll', function () {
            scrollPos = $(this).scrollTop();

            if (scrollPos > 1) {
                $('.navbar').addClass('navbar-alt');
            } else {
                $('.navbar').removeClass('navbar-alt');
            }
        });
    }

    function scrollToNavigate() {
        $('.scroll-to').on('click', function (e) {

            var targetHref = $(this).attr('href'),
                headerHeight = $('header').outerHeight();

            $('html, body').animate({
                scrollTop: $(targetHref).offset().top - headerHeight
            }, 1200);

            e.preventDefault();
        });
    }

    function responsiveNavigation() {
        $('.check-menu').on('click', function () {
            $('.collapsible').toggleClass('collapsed');
            $('body').toggleClass('no-scroll');
        });

        $('.menu.navigation>li>a, .demo-btn').on('click', function () {
            $('.check-menu').prop('checked', false);
            $('.collapsible').removeClass('collapsed');
            $('body').removeClass('no-scroll');
        });
    }

    function uncheckResponsiveMenuBtn() {
        $(window).on('load resize', function () {
            if (this.innerWidth >= 1024) {
                $('.check-menu').prop('checked', false);
                $('.collapsible').removeClass('collapsed');
                $('body').removeClass('no-scroll');
            }
        });
    }
    
    // Timeline Slideshow
    var MAX_DOTS = 5;
    var INITIAL_DOT = 0;
    var ACTUAL_INDEX = 0;
    var TIMEOUT_BETWEEN_TRANSITIONS = 7000;
    var currentTimeout = null;

    function addActiveClassToRounds(index) {
        for (var counter = 1; counter <= index; counter++) {
            $(".round-".concat(counter)).addClass("active");
        }
    }

    function removeCurrenClassFromPreviousRounds(index) {
        for (var counter = 1; counter < index; counter++) {
            $(".round-".concat(counter)).removeClass("current");
        }
    }

    function setDotActive(index) {
        $(".dots li").removeClass("active");
        $(".dots li").removeClass("current");
        $(".slide-section").removeClass("active");
        addActiveClassToRounds(index);
        removeCurrenClassFromPreviousRounds(index);
        $(".round-".concat(index)).addClass("current");
        $(".slide-".concat(index)).addClass("active");
    }

    function setStartTimelineTransitionFrom(index) {
        setDotActive(index + 1);

        if (currentTimeout) {
            clearTimeout(currentTimeout);
        }
        ACTUAL_INDEX = index + 1;
        currentTimeout = setTimeout(function () {
            ACTUAL_INDEX = (ACTUAL_INDEX === MAX_DOTS)
                ? 0
                : ACTUAL_INDEX
            setStartTimelineTransitionFrom(ACTUAL_INDEX)
        }, TIMEOUT_BETWEEN_TRANSITIONS);
    }

    function horizontalTimelineSlider() {
        setStartTimelineTransitionFrom(0);
        
        var _setClickableButtons = function _setClickableButtons(counter) {
            $(".round-" + counter).on("click", function () {
                setStartTimelineTransitionFrom(counter - 1);
            });
        };
        
        for (var counter = INITIAL_DOT; counter <= MAX_DOTS; counter++) {
            _setClickableButtons(counter);
        }
    }
        
    stickyNavigation();
    scrollNavigation();
    scrollToNavigate();
    responsiveNavigation();
    uncheckResponsiveMenuBtn();
    horizontalTimelineSlider();
});