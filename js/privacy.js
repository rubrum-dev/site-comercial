"use strict";

$(function () {
    $('.demo, .request-budget').on('click', function () {
        $.confirm({
            title: '',
            content: '' +
                '<form action="#" method="POST" autocomplete="off" name="formDemo" id="formDemo" class="form-demo form-container">' +
                '<h3 class="txt-size-lg txt-x-bold txt-center margin-bottom-20">Uma demonstração personalizada para você</h3>' +
                '<p class="txt-size-sm txt-regular txt-center margin-bottom-40">Entraremos em contato para agendar uma demonstração do Rubrum e te enviar um orçamento.</p>' +
                '<div class="flexbox-container flexbox-group">' +
                '<div class="demo-img-left flex flex-6-large">' +
                '<figure>' +
                '<img src="/images/layout/sistema-desktop-demo.png" class="img-fluid" />' +
                '</figure>' +
                '</div>' +
                '<div class="form-content flex flex-fluid">' +
                '<div class="form-group flex-12-large">' +
                '<input type="text" placeholder="Nome" name="nome" id="nome" class="input-control " />' +
                '</div>' +
                '<div class="form-group flex-12-large">' +
                '<input type="text" placeholder="Empresa" name="empresa" id="empresa" class="input-control" />' +
                '</div>' +
                '<div class="form-group flex-12-large">' +
                '<input type="email" placeholder="E-mail" name="email" id="email" class="input-control" />' +
                '</div>' +
                '<div class="form-group flex-12-large">' +
                '<input type="tel" placeholder="Telefone" name="telefone" id="telefone" class="input-control" />' +
                '</div>' +
                '<div class="form-group flex-12-large recaptcha-container">' +
                '<div id="recaptcha1" class="recaptcha"></div>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</form>',
            animateFromElement: false,
            animation: 'scale',
            backgroundDismiss: true,
            closeIcon: true,
            closeIconClass: 'jconfirm-btn-close fa fa-times',
            containerFluid: false,
            columnClass: '',
            buttons: {
                formSubmit: {
                    text: '<span><i class="icon to-right fa fa-paper-plane"></i> Enviar Solicitação</span>',
                    btnClass: 'btn-enviar btn btn-block btn-default',
                    keys: ['enter'],
                    action: function () {
                        var nome = $('#nome').val(),
                            empresa = $('#empresa').val(),
                            email = $('#email').val(),
                            telefone = $('#telefone').val(),
                            emailRegex = /^([a-z0-9_\.\-])+\@(([a-z0-9\-])+\.)+([a-z0-9]{2,4})+$/,
                            errorDOM = $('<label></label>');

                        $(errorDOM).addClass('error');

                        if (nome === '') {
                            $('label.error').remove();
                            $('#nome').addClass('error');
                            $('#nome').after(errorDOM);
                            $(errorDOM).text('Nos diga o seu nome.');

                            return false;
                        } else if (empresa === '') {
                            $('label.error').remove();
                            $('#empresa').addClass('error');
                            $('#empresa').after(errorDOM);
                            $(errorDOM).text('Nos diga a sua empresa.');

                            return false;
                        } else if (email === '' || !emailRegex.test($('#email').val())) {
                            $('label.error').remove();
                            $('#email').addClass('error');
                            $('#email').after(errorDOM);
                            $(errorDOM).text('Nos diga o seu e-mail.');

                            return false;
                        } else if (telefone === '') {
                            $('label.error').remove();
                            $('#telefone').addClass('error');
                            $('#telefone').after(errorDOM);
                            $(errorDOM).text('Nos diga o seu telefone.');

                            return false;
                        }

                        var v = grecaptcha.getResponse();

                        if (v.length == 0) {
                            $('label.error').remove();
                            $('#recaptcha1').after(errorDOM);
                            $(errorDOM).text("Preencha o Captcha.");

                            return false;
                        }

                        $('.lds').show();

                        $.ajax({
                            type: 'POST',
                            url: 'sendmail.php',
                            data: {
                                nome: nome,
                                empresa: empresa,
                                email: email,
                                telefone: telefone,
                                captcha: v
                            }, // or JSON.stringify ({name: 'jonas'}),
                            success: function (data) {
                                if (data.success) {

                                    $('#recaptcha1').empty();

                                    $('.lds').hide();

                                    $.alert({
                                        title: 'Solicitação enviada, obrigado!',
                                        content: '' +
                                            '<p class="txt-size-sm txt-center margin-bottom-40">Nos falaremos em breve.</p>' +
                                            '<div class="flexbox-container flexbox-group flex-justify-center">' +
                                            '<span class="jconfirm-success-sign fa fa-smile-o"></span>' +
                                            '</div>',
                                        containerFluid: false,
                                        columnClass: '',
                                        buttons: {
                                            entendi: {
                                                text: '<span><i class="icon to-right fa fa-thumbs-up"></i> Combinado</span>',
                                                keys: ['enter'],
                                                action: function () {}
                                            }
                                        },
                                        onOpenBefore: function () {
                                            $('body').addClass('no-scroll');
                                            $('.privacidade-master-wp').addClass('blurred');
                                            $('.jconfirm').css('overflow', 'auto');
                                            $('.jconfirm-buttons').addClass('no-float txt-center');
                                        },
                                        onOpen: function () {
                                            $('body').addClass('no-scroll');
                                            $('.privacidade-master-wp').addClass('blurred');
                                            $('.jconfirm').css('overflow', 'auto');
                                            $('.jconfirm-buttons').addClass('no-float txt-center');
                                        },
                                        onDestroy: function () {
                                            $('body').removeClass('no-scroll');
                                            $('.privacidade-master-wp').removeClass('blurred');
                                            $('.jconfirm').css('overflow', 'hidden');
                                            $('.jconfirm-buttons').removeClass('no-float txt-center');
                                        }
                                    });

                                } else {
                                    $('.lds').hide();
                                    $('#recaptcha1').empty();

                                    $.alert({
                                        title: 'Erro',
                                        content: '' +
                                            '<p class="txt-size-sm txt-center margin-bottom-40">' + data.message + '</p>' +
                                            '<div class="flexbox-container flexbox-group flex-justify-center">' +
                                            '<span class="jconfirm-success-sign fa fa-frown-o"></span>' +
                                            '</div>',
                                        containerFluid: false,
                                        columnClass: '',
                                        buttons: {
                                            entendi: {
                                                text: '<span><i class="icon to-right fa fa-check"></i> Ok</span>',
                                                keys: ['enter'],
                                                action: function () {}
                                            }
                                        },
                                        onOpenBefore: function () {
                                            $('body').addClass('no-scroll');
                                            $('.privacidade-master-wp').addClass('blurred');
                                            $('.jconfirm').css('overflow', 'auto');
                                            $('.jconfirm-buttons').addClass('no-float txt-center');
                                        },
                                        onOpen: function () {
                                            $('body').addClass('no-scroll');
                                            $('.privacidade-master-wp').addClass('blurred');
                                            $('.jconfirm').css('overflow', 'auto');
                                            $('.jconfirm-buttons').addClass('no-float txt-center');
                                        },
                                        onDestroy: function () {
                                            $('body').removeClass('no-scroll');
                                            $('.privacidade-master-wp').removeClass('blurred');
                                            $('.jconfirm').css('overflow', 'hidden');
                                            $('.jconfirm-buttons').removeClass('no-float txt-center');
                                        }
                                    });

                                }

                            },
                            //contentType: "application/json",
                            dataType: 'json'
                        });

                    }
                }
            },
            onContentReady: function () {
                var jc = this;

                this.$content.find('form').on('submit', function (e) {
                    e.preventDefault();

                    jc.$$formSubmit.trigger('click');
                });

                $(document).on('focus', 'input.error', function () {
                    $('label.error').remove();
                });

                $(document).on('click', 'label.error', function () {
                    $(this).remove();
                    $('input.error').focus();
                });

                var SPMaskBehavior = function (val) {
                        return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
                    },
                    spOptions = {
                        onKeyPress: function (val, e, field, options) {
                            field.mask(SPMaskBehavior.apply({}, arguments), options);
                        }
                    };

                $('#telefone').mask(SPMaskBehavior, spOptions);

                grecaptcha.render("recaptcha1", {
                    sitekey: "6Lf5skgaAAAAAFQNIlBwFrdQOYXTRBAnesQYpFw9",
                    theme: "light"
                });
            },
            onOpenBefore: function () {
                $('body').addClass('no-scroll');
                $('.jconfirm-box').addClass('jc-demo-container');
                $('.privacidade-master-wp').addClass('blurred');
            },
            onDestroy: function () {
                $('body').removeClass('no-scroll');
                $('.jconfirm-box').removeClass('jc-demo-container');
                $('.privacidade-master-wp').removeClass('blurred');
                $('#recaptcha1').empty();
            }
        });
    });

    AOS.init({
        offset: 40,
        once: true
    });

    $('.header-in').visualNav({
        link: 'a.menu-link',
        useHash: false,
        bottomMargin: 100,
        offsetTop: 0,
        animationTime: 800,
        changed: function(api) {}
    });

    $('.sidebar-privacidade-ul').visualNav({
        link: 'a.privacidade-link',
        useHash: false,
        bottomMargin: 100,
        offsetTop: 100,
        animationTime: 800,
        changed: function(api) {}
    });

    setTimeout(() => {
        history.replaceState('', document.title, window.location.origin + window.location.pathname + window.location.search);
    }, 5);

    function responsiveNavigation() {
        $('.check-menu').on('click', function () {
            $('.collapsible').toggleClass('collapsed');
            $('body').toggleClass('no-scroll');
        });

        $('.menu.navigation>li>a, .demo-btn').on('click', function () {
            $('.check-menu').prop('checked', false);
            $('.collapsible').removeClass('collapsed');
            $('body').removeClass('no-scroll');
        });
    }

    function uncheckResponsiveMenuBtn() {
        $(window).on('load resize', function () {
            if (this.innerWidth >= 1024) {
                $('.check-menu').prop('checked', false);
                $('.collapsible').removeClass('collapsed');
                $('body').removeClass('no-scroll');
            }
        });
    }

    responsiveNavigation();
    uncheckResponsiveMenuBtn();
});