/* MIT License */
/* Copyright (c) 2019 Maykheld (http://www.maykheld.com) */
jQuery.fn.extend({
    WAFloatBox: function () {
        var a = $(this)
        a.prepend(`
        <div id="suporte" class="myk-btn">
            <i class="myk-wa-icon fa fa-whatsapp"><span></span></i>
        </div>
        <div class="myk-panel"></div>
        `)

        var b = $(".myk-panel")
        b.append(`
        <div class="myk-panelhead">
            <i class="myk-wa-icon fa fa-whatsapp"></i>
            <h2 class="myk-paneltitle">Dúvidas?</h2>
            <small class="myk-panelsubtitle">Clique em um colaborador para falar via WhatsApp.</small>
        </div>
        <div class="myk-panelbody">
            <p class="myk-panel-notice">Nossa equipe normalmente responde em poucos minutos.</p>
        </div>
        `)

        $('.myk-item').each(function () {
                //this wrapped in jQuery will give us the current .letter-q div
                $('.myk-panelbody').append(`
            <a target='_blank' href="https://wa.me/${$(this).data('wanumber')}" class="myk-list">
                <span class="myk-ava-icon fa fa-user"><img src="${$(this).data('waava')}" onerror="this.style.display='none'" class="myk-ava"></span>
                <div class="myk-number">
                    <p class="myk-name txt-size-sm">${$(this).data('waname')}</p>
                    <p class="myk-who txt-size-xs"><b>${$(this).data('wadivision')}</b></p>
                </div>
            </a>
            `)
        });

        // trigger 
        $(document).on('click', '.myk-wa.aos-animate .myk-btn', function (e) {
            e.preventDefault();
            e.stopPropagation();
            $(".myk-panel").toggleClass("myk-show");
            $(".myk-btn").toggleClass("active");
        });
    }
});